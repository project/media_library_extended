## Media Library Extended

The Media Library Extended module is an API module that provides plugins and
configuration that allow other modules to integrate with Drupal core's Media
Library.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_library_extended

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/media_library_extended

### Requirements

This module requires the following modules:

 * Media Library (starting with Drupal 8.7.0)

### Extended modules

By itself, this module is not very useful. You either need to write a custom
MediaLibrarySource plugin for your service, or use one provided by the
community.

### Install

 * Install as you would normally [install a contributed Drupal module](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).

#### Installing via Composer


### Usage/Configuration

Configure available Media Library Panes in Administration » Configuration »
Media » Media library » Panes

Depending on the selected Media bundle, different source plugins may be
available.

Source plugins may provide configurations options that are specific to the
pane being configured.

Each configured pane is shown as a Tab in the Media Library whenever the
associated media bundle is available.
